<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
	public $timestamps = false;
	
    protected $fillable = [
    	'ip_id', 'browser_id', 'short_id'
    ];



    //		-- Relationships --
    
    public function browser() {
    	return $this->belongsTo('App\Browser');
    }

    public function short() {
    	return $this->belongsTo('App\Short');
    }

    public function ip() {
    	return $this->belongsTo('App\IP', 'ip_id');
    }
    
}
