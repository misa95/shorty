<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UrlShortenedMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    public $shortenedUrl, $original, $link;

    public function __construct($shortened, $original, $overviewLink)
    {
        $this->shortenedUrl = $shortened;
        $this->original = $original;
        $this->link = $overviewLink;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Short URL generated');
        return $this->view('email.generated');
    }
}
