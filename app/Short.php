<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Short extends Model
{

	public $timestamps = false;

	protected $fillable = [
		'shortened', 'original', 'user_id'
	];



	//		-- Accessors --

	public function getUrlAttribute() {
		return route('visit', ['code' => $this->shortened]);
	}

	public function getOverviewLinkAttribute() {
		return route('overview', ['code' => $this->shortened]);
	}


    
	//		-- Cusom methods --

	public static function generateCode() {
		do {
			$code = str_random(5);
		} while ( static::where('shortened', $code)->exists() );

		return $code;
	}



	//		-- Relationships --
	
	public function visits() {
		return $this->hasMany('\App\Visit');
	}
}
