<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Browser extends Model
{
    public $timestamps = false;
    
    protected $table = 'browser_marks';

    protected $fillable = [
    	'mark'
    ];



    //		-- Custom methods --
    		
    public static function generateMark() {
    	do {
			$mark = str_random(5);
		} while ( static::where('mark', $mark)->exists() );

		return $mark;
    }
}
