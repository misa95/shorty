<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\GenerateUrlRequest;

use App\Jobs\SendMail;

use App\Short;

class URLController extends Controller
{
    
	public function generate(GenerateUrlRequest $req) {
		$db = resolve('db');
		$db->beginTransaction();

		$code = Short::generateCode();
		$short = new Short([
			'original' => $req->url,
			'shortened' => $code
		]);

		if ( !is_null( $req->email ) ) {
				try {
					$user = \App\User::firstOrCreate( $req->only('email') );
				} 
				catch (\Illuminate\Database\QueryException $e) {
					$db->rollback();
					return redirect()->back()->withErrors(['database' => 'Something went wrong']);
				}
				catch(Exception $e) {
					$db->rollback();
					return redirect()->back()->withErrors(['database' => 'Something went wrong']);
				}
			}
			$short->user_id = $user->id;

		try {
			$short->save();
		} 
		catch (\Illuminate\Database\QueryException $e) {
			$errorCode = $e->errorInfo[1];
			$message = ( $errorCode == 1062 ) ? 'You have already generated a short URL for this link' : 'Something went wrong';
			return redirect()->back()->withErrors(['database' => $message]);
		}
		catch(Exception $e) {
			$db->rollback();
			return redirect()->back()->withErrors(['database' => $e->getMessage()]);
		}

		$db->commit();
		
		if ( !is_null($req->email) ) {
			$email = new \App\Mail\UrlShortenedMail($short->url, $short->original, $short->overviewLink);
			resolve('mailer')->to( $req->email )->send($email);

			// Zbog lakoće postavljanja sam ostavio da se mail šalje neposredno iz kontrolera, a ne iz queue-a
			// Za testiranje slanja mailova pomoću queue-a zakomentarisati liniju iznad a otkomentarisati liniju ispod ovog komentara i iz komandne linije u root folderu projekta pokrenuti sledeću komandu 
			// "php artisan queue:work --queue=emailing --tries=5". 
			// SendMail::dispatch($email, $req->email)->onQueue('emailing');
		}

		$successMessage = 'Short URL generated. Your URL is: <b>' . $short->url . '</b>';
		return redirect('/')->with(['message' => $successMessage]);
	}

}
