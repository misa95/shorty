<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Visit;
use App\Short;
use App\IP;
use App\Browser;

class VisitController extends Controller
{
    
	public function visit(Request $req, $code) {
		$short = Short::where('shortened', $code)->first();

		if ( !$short )
			abort(404);

		$response = redirect( $short->original );

		// IP adrese čuvamo ukoliko postoji potreba da korisniku damo uvid u to sa kojih adresa i/ili lokacija je njegov link posećivan
		$ip = IP::firstOrCreate( ['ip' => $req->ip()] );

		$browserMark = $req->cookie('browser_mark');
		if ( is_null($browserMark) )
			$browserMark = Browser::generateMark();
		$browser = Browser::firstOrCreate( ['mark' => $browserMark] );
		
		$response->cookie( 'browser_mark', $browserMark, now()->addYears(5)->diffInMinutes( now() ) );


		$visit = Visit::firstOrNew([
			'ip_id' => $ip->id,
			'browser_id' => $browser->id,
			'short_id' => $short->id
		]);

		if ($visit->exists)
			$visit->count++;

		$visit->last_visit = now();
		$visit->save();

		return $response;
	}

	public function visitOverview($code) {
		$short = Short::where('shortened', $code)->first();

		if ( !$short )
			return redirect('generate');

		$unique = $short->visits()->count( resolve('db')->raw('DISTINCT(browser_id)') );
		$total = $short->visits()->sum('count');
		return view( 'overview', compact('unique', 'total', 'short') );
	}
    
}
