<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('generate');
});


/**
 *  Request for generating a shortened URL for the provided full URL.
 *  Emails the user if a email address is provided and saves user info in the database.
 */
Route::post('generate', 'URLController@generate');


/**
 *	Takes note of the user's visit, stores info about it in the database and then redirects the user to the full URL.
 */
Route::get('/{code}', 'VisitController@visit')->name('visit');


/**
 * 	Displays usage statistics about a generated short URL
 */
Route::get('overview/{code}', 'VisitController@visitOverview')->name('overview');