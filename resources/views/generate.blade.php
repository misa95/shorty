<!DOCTYPE html>
<html>
<head>
	<title>Shorty | No such thing as too short</title>
</head>
<body>
	<h2>Generate a shortened URL</h2>
	@if ($errors->any())
		<p>
			There were some errors. Please fix these:
			<ul>
				@foreach($errors->all() as $error)
					<li>
						{{$error}}
					</li>
				@endforeach
			</ul>
		</p>
	@endif
	@if ( Session::has('message') )
		<p>
			{!! session('message') !!}
		</p>
	@endif
	<p>
		{!! Form::open(['url' => 'generate']) !!}

			<p>
				URL* 
				<br>
				<input type="url" name="url" required placeholder="Enter a URL to shorten">
			</p>

			<p>
				Your e-mail address:
				<br>
				{!! Form::email('email', null, ['placeholder' => 'Enter your email']) !!}
			</p>

			<p>
				{!! Form::submit('Generate') !!}
			</p>

		{!! Form::close() !!}
	</p>
</body>
</html>