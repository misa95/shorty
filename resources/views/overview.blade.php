<!DOCTYPE html>
<html>
<head>
	<title>Shorty | Overview</title>
</head>
<body>
	<h1>
		Short URL statistics
	</h1>
	<p>
		Original URL:
		{{ $short->original }}
	</p>
	<p>
		Short url:
		{{ $short->url }}
	</p>
	<p>
		Total visits:
		{{ $total }}
	</p>
	<p>
		Unique visits:
		{{ $unique }}
	</p>
</body>
</html>