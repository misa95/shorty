<h2>
	Short URL generated
</h2>
<p>
	Dear User,
</p>
<p>
	<br>
	You have generated a short URL leading to this address:
	<b>
		{{ $original }}
	</b>
</p>
<p>
	This is the short URL you have generated: 
	<b>
		{{ $shortenedUrl}}
	</b>
</p>
<p>
	You may view usage reports about your URL at <a href="{{ $link }}">this link</a>
</p>
<p>
	Best regards,
	<br>
	Shorty team
</p>