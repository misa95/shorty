<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VisitMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('short_id')->unsigned();
            $table->foreign('short_id')
                  ->references('id')
                  ->on('shorts')
                  ->onDelete('cascade');

            $table->integer('ip_id')->unsigned();
            $table->foreign('ip_id')
                  ->references('id')
                  ->on('ips')
                  ->onDelete('restrict');

            $table->integer('browser_id')->unsigned();
            $table->foreign('browser_id')
                  ->references('id')
                  ->on('browser_marks')
                  ->onDelete('restrict');

            $table->integer('count')->default(1);

            $table->timestamp('first_visit')->useCurrent();
            $table->timestamp('last_visit')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
