<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BrowserMarkMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('browser_marks', function (Blueprint $table) {
            $table->increments('id');
            

            $table->string('mark')->unique();

            $table->integer('added_by')->unsigned()->nullable();
            $table->foreign('added_by')
                  ->references('id')
                  ->on('ips')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('browser_marks');
    }
}
